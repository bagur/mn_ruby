BEGIN TRANSACTION;
DROP TABLE IF EXISTS Clients;
CREATE TABLE Clients (
    'Id' INTEGER PRIMARY KEY AUTOINCREMENT,
    'Name' VARCHAR NOT NULL,
    'Contacts' VARCHAR NOT NULL UNIQUE,
    'DateStamp' VARCHAR NOT NULL,
    'Barber' VARCHAR DEFAULT NULL,
    'Color' VARCHAR DEFAULT NULL
);

DROP TABLE IF EXISTS Contacts;
CREATE TABLE Contacts (
    'Id' INTEGER PRIMARY KEY AUTOINCREMENT,
    'Email' VARCHAR NOT NULL,
    'Message' TEXT NOT NULL
);

INSERT INTO "Clients" ('Name', 'Contacts', 'DateStamp', 'Barber', 'Color')
    VALUES('Juno', '12345678901','04/13 14:20', 'Gus Fring', '#7bd148');

INSERT INTO "Contacts" ('Email', 'Message')
    VALUES('neo@mail.io', 'White rabbit was here...');

COMMIT;
