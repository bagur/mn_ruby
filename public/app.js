function some_method () {
    let x = window.localStorage.getItem('bbbb');
    x = x * 1 + 1;
    window.localStorage.setItem('bbbb' , x);
    alert(x);
}

function add_to_cart (id) {
    // add position to cart
    let key = 'product_' + id;
    let x = window.localStorage.getItem(key);
    x = x * 1 + 1;
    window.localStorage.setItem(key, x);
    update_orders_input();
    console.log("Pizza_id: " + key + ", q-ty: "+ x);
    console.log(cart_get_orders());
    update_orders_button();
}

function update_orders_input() {
    // body...
    var orders = cart_get_orders();
    $("#orders_input").val(orders);
}

function update_orders_button () {
    // body...
    var text = "[" + show_cart_items() + "]";
    $("#orders_button").html(text);
}

function show_cart_items () {
    // show cart
    var cnt = 0;
    for (let i = 0; i < window.localStorage.length; i++ ) {
        let key = window.localStorage.key(i);
        let value = window.localStorage.getItem(key);
        if(key.indexOf('product_') == 0){
            // ...
            cnt = cnt + value*1;
        }
    };
    console.log("Total amount of pizza: " + cnt);
    return cnt;
}

function cart_get_orders () {
    // show cart
    var orders = '';
    for (let i = 0; i < window.localStorage.length; i++ ) {
        let key = window.localStorage.key(i);
        let value = window.localStorage.getItem(key);
        if(key.indexOf('product_') == 0){
            // ...
            orders = orders + key + '=' + value + ',';
        }
    };
    return orders;
}
