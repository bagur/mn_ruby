// hello
// .selector -> class="selector"
// #selector -> id="selector"
function hello_js (str) {
    console.log(str);
}

hello_js('Hello mutie!');
hello_js('.selector -> class="selector"');
hello_js('#selector -> id="selector"');
