#!/usr/bin/env ruby
# encoding: utf-8
# web dev demo Pizza::Nest

require 'rubygems'
require 'bcrypt'
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/activerecord'

class Product < ActiveRecord::Base
end

configure do
	set :database, 'sqlite3:db/pizza.db'
  set :bind, '0.0.0.0'
  set :port, 8080
  enable :sessions
end

get '/' do
  # ... 
  @page = {
    class: 'alert alert-info',
    title: 'Ola Pizza!',
    message: 'Every slice you take ...',
    nav_home: 'active',
    nav_cart: 'active'
  }
  @products = Product.all
  # erb :message
  erb :index
end

get '/about' do
  #
  @page = {
    class: 'alert alert-info',
    title: 'About us',
    message: 'Welcome at <mark>Pizza::Nest</mark> site.',
    nav_about: 'active'
  }
  # erb :home
  erb :index 
end

get '/cart' do
  # ...
  @page = {
    class: 'alert alert-info',
      title: 'Cart',
      message: 'You are in cart.',
      nav_cart: 'active'
    }
  # erb :home
  erb :cart
end

post '/cart' do
  # ...
  @page = {
    class: 'alert alert-info',
    title: 'Cart',
    message: 'You are in cart.',
    nav_cart: 'active'
  }
  # erb :home
  erb :cart
end