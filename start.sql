PRAGMA foreign_keys = ON;
CREATE TABLE IF NOT EXISTS Roles (
    'id' INTEGER PRIMARY KEY AUTOINCREMENT,
    'role' VARCHAR NOT NULL UNIQUE,
    'active' BOOLEAN NOT NULL default 1 CHECK (active IN (0,1))
);
CREATE TABLE IF NOT EXISTS Users (
    'id' INTEGER PRIMARY KEY AUTOINCREMENT,
    'name' VARCHAR NOT NULL,
    'username' VARCHAR NOT NULL UNIQUE,
    'passwd' VARCHAR NOT NULL,
    'role_id' INTEGER NOT NULL,
    'lastlogin' TIMESTAMP DEFAULT (strftime('%Y-%m-%d %H:%M:%S','now', 'localtime')) NOT NULL,
    'counter' INTEGER NOT NULL DEFAULT 0,
    FOREIGN KEY (role_id) REFERENCES Roles (id)
);
CREATE TABLE IF NOT EXISTS Clients (
    'id' INTEGER PRIMARY KEY AUTOINCREMENT,
    'name' VARCHAR NOT NULL,
    'contacts' VARCHAR NOT NULL UNIQUE,
    'datestamp' VARCHAR NOT NULL,
    'barber' VARCHAR DEFAULT NULL,
    'color' VARCHAR DEFAULT NULL
);
CREATE TABLE IF NOT EXISTS Contacts (
    'id' INTEGER PRIMARY KEY AUTOINCREMENT,
    'email' VARCHAR NOT NULL,
    'message' TEXT NOT NULL
);
INSERT OR IGNORE INTO 'Roles' (role) VALUES('admin');
INSERT OR IGNORE INTO 'Roles' (role) VALUES('manager');
INSERT OR IGNORE INTO 'Roles' (role) VALUES('barber');
INSERT OR IGNORE INTO 'Roles' (role, active) VALUES('guest', 0);
INSERT OR IGNORE INTO 'Users'
    (name, username, passwd, role_id)
VALUES
    ('Admin', 'admin@barber.shop', '$2a$10$fYW9khBEyFubFMQEyu2MT.oSupU3pkFlgjG7nsOsPP53l3XOHNtc.', (SELECT id FROM Roles WHERE role = 'admin'));
INSERT OR IGNORE INTO 'Users'
    (name, username, passwd, role_id)
VALUES
    ('Walter White', 'walter@barber.shop', '$2a$10$Pln8bjvKqE3loCGrPcHWg.tkYQpllOcCJyNM1QNEg.4ggc32brY9i', (SELECT id FROM Roles WHERE role = 'barber'));
INSERT OR IGNORE INTO 'Users'
    (name, username, passwd, role_id)
VALUES
    ('Jessie Pinkman', 'jessie@barber.shop', '$2a$10$i7Hbo32LGYUi3YbxiuQGW.ZWvyDvn/PxjFbErJ.qcj95D7v6wpTqe', (SELECT id FROM Roles WHERE role = 'barber'));
INSERT OR IGNORE INTO 'Users'
    (name, username, passwd, role_id)
VALUES
    ('Gus Fring', 'gus@barber.shop', '$2a$10$LzZBbjuYcaTvVpFcHuVjkOaB9pheBvVc/PVCN8OswoqyAPOK5pSua', (SELECT id FROM Roles WHERE role = 'barber'));