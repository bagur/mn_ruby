class SeedPoducts < ActiveRecord::Migration[5.2]
  def change
    Product.create(
      title: 'hawaiian',
      description: 'Hawaiian style pizza',
       size: 32,
       price: 499,
       is_spicy: false,
       is_veg: false,
       is_best_offer: false,
       image: '/images/hawaiian.png'
    )
    Product.create(
      title: 'pepperoni',
      description: 'Natural Pepperoni pizza',
       size: 32,
       price: 449,
       is_spicy: true,
       is_veg: false,
       is_best_offer: false,
       image: '/images/pepperoni.png'
    )
    Product.create(
      title: 'vegeterian',
      description: 'Light Vegeterian style pizza',
       size: 32,
       price: 399,
       is_spicy: false,
       is_veg: true,
       is_best_offer: false,
       image: '/images/vegeterian.png'
    )
  end
end
