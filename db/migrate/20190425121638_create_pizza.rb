class CreatePizza < ActiveRecord::Migration[5.2]
  def change

    create_table :products do |p|
      p.text :title
      p.text :description 
      p.integer :size
      p.integer :price
      p.boolean :is_spicy
      p.boolean :is_veg
      p.boolean :is_best_offer
      p.string :image

      p.timestamps
    end
  end
end
