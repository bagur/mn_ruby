#!/usr/bin/env ruby
# encoding: utf-8
# web dev demo BarberShopNext
@debug = false

require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/activerecord'


class Clients < ActiveRecord::Base
end


### _methods
def db_conn db='barbershop.db'
    # return dabase connection
    puts "ECHO[db_conn]: #{db}" if @debag # extended out put
    #
    require 'sqlite3'
    #
    db = SQLite3::Database.new db
    db.results_as_hash = true
    return db
end

def get_request hh = {sql: "SELECT name FROM sqlite_master WHERE type = 'table';", }
    # return array or false
    print "ECHO[get_request]: #{hh}\n" if @debag # extended out put
    db = db_conn()
    r = db.execute( hh[:sql] )
    db.close
    puts r if @debag # extended out put
    return r.size > 0 ? r : false
end


### _getters
def get_roles hh = {sql: 'SELECT * FROM Roles', }
    # return array of roles or false
    print "ECHO[get_roles]: #{hh}\n" if @debag # extended out put
    get_request( hh )
end

def get_users hh = {sql: 'SELECT Users.id as id, name, username, Roles.role AS role, lastlogin, counter FROM Users LEFT JOIN Roles ON (role_id = Roles.id);', }
    # return array of users or false
    print "ECHO[get_users]: #{hh}\n" if @debag # extended out put
    get_request( hh )
end

def get_barbers hh = {sql: 'SELECT name FROM Users WHERE role_id = (SELECT id FROM Roles WHERE role = "barber");'}
    # return array of barbers or false
    print "ECHO[get_barbers]: #{hh}\n" if @debag # extended out put
    get_request( hh )
end

def get_clients hh = {sql: 'SELECT * FROM Clients ORDER BY datestamp ASC;'}
    # return array of clients or false
    print "ECHO[get_clients]: #{hh}\n" if @debag # extended out put
    get_request( hh )
end

def get_contacts hh = {sql: 'SELECT * FROM Contacts ORDER BY id DESC;'}
    # return array of contacts or false
    print "ECHO[get_contacts]: #{hh}\n" if @debag # extended out put
    get_request( hh )
end

def check_auth? user
    # check authenticated user
    require 'bcrypt'
    #
    @result = false
    get_request( {sql: "SELECT passwd FROM Users WHERE username = '#{user[:username]}'", } ) do |rec|
        passwd = BCrypt::Password.new("#{rec['passwd']}")
        @result = (passwd == user[:passwd])
    end
end


### _setters
def save_clients client
    #
    values = [client[:name], client[:contacts], client[:datestamp], client[:barber], client[:color]]
    db = db_conn()
    db.execute( 'INSERT INTO Clients (name, contacts, datestamp, barber, color) VALUES (?, ?, ?, ?, ?)', values )
    db.close
end

def save_contacts feedback
    #
    values = [feedback[:email], feedback[:message]]
    db = db_conn()
    db.execute( 'INSERT INTO Contacts (email, message) VALUES (?, ?)', values )
    db.close
end

def upd_lastlogin username
    # update field(lastlogin) for user record in table users
    print "ECHO[upd_lastlogin]: #{username}\n" if @debag # extended out put
    users = get_users( {sql: 'SELECT username FROM Users;'} )
    if users.include?( {'username' => username} )
        print "ECHO[upd_lastlogin]: #{username}\n" if @debag # extended out put
        get_request( {sql: "UPDATE Users SET lastlogin = strftime('%Y-%m-%d %H:%M:%S','now', 'localtime'), counter = (counter + 1) WHERE username = '#{username}';"} )
    end
end


### _configure
configure do
    #set :views [ './views/a', './views/b' ]
    set :bind, '0.0.0.0'
    set :port, 8080
    enable :sessions
    # get queries FROM file
    f = File.open('./start.sql', 'r')
    sql = f.read
    f.close
    # put queries into db
    require 'sqlite3'
    db = SQLite3::Database.new './barbershop.db'
    db.execute_batch( sql )
    db.close
end


### _helpers
helpers do
    def username
        session[:identity] ? session[:identity] : ''
    end
end


### _before
before '/secure/*' do
    unless session[:identity]
        session[:previous_url] = request.path
        @class = 'alert alert-warning'
        @title = '<strong>Access denied</strong>!'
        @message = 'Sorry, you need to be logged in to visit <strong>"' + request.path + '"</strong>.'
        erb :message
        halt erb(:login_form)
    end
end


### _root
get '/' do
    @class = 'alert alert-info'
    @title = 'Welcome'
    @message = 'Can you handle a <a href="/secure/place" class="alert-link">secret</a>?'
    @nav_home = 'active'
    erb :message
end

get '/login/form' do
    @class = 'alert alert-info'
    @title = 'Welcome'
    @message = 'Can you handle a <a href="/secure/place" class="alert-link">secret</a>?'
    erb :login_form
end

post '/login/attempt' do
    @login = params['username']
    passwd = params['passwd']

    if check_auth?({username: @login, passwd: passwd})
        session[:identity] = get_users( { sql: "SELECT name FROM Users WHERE username = '#{@login}';" } )[0]['name']
        upd_lastlogin( @login )
        redirect to '/secure/clients'
    end

    # redirect to session[:previous_url] || '/'

    @class = 'alert alert-warning'
    @title = '<strong>Access denied</strong>!'
    @message = 'Wrong login or password.'
    erb :message
    halt erb(:login_form)
end
  
get '/logout' do
    session.delete(:identity)
    params[:username] = nil
    @class = 'alert alert-warning'
    @title = 'Now you logged out.'
    @message = 'Bye!'
    erb :message
end

get '/about' do
    @class = 'alert alert-info'
    @title = 'About us'
    @message = "We start creating <mark>BARBERSHOP</mark> site."
    @nav_about = 'active'
    erb :message
end


### _visit
get '/visit' do
    @class = 'alert alert-info'
    @title = 'Записаться на стрижку'
    @message = 'Please choose your barber:'
    @nav_visit = 'active'
    @barbers = get_barbers()

    erb :visit
end

post '/visit' do
    @name = params[:name]
    @datestamp = params[:datestamp]
    @barber = params[:barber]
    @contacts = params[:contacts]
    @color = params[:color]
    
    @class = 'alert alert-success'
    @title = "Thank you"
    @message = "Dear <b>#{@name}</b>, we're waiting for you at <b>#{@datestamp}</b>.<br />Your barber <b>#{@barber}</b>."
    @nav_visit = 'active'

    save_clients({
        name: @name,
        contacts: @contacts,
        datestamp: @datestamp,
        barber: @barber,
        color: @color,
    })

    erb :message
end


### _contacts
get '/contacts' do
    @class = 'alert alert-info'
    @title = 'Contacts'
    @message = 'Here you may leave a note.'
    @nav_contacts = 'active'
    erb :contacts
end

post '/contacts' do
    @email = params[:email]
    @note = params[:note]
    @class = 'alert alert-success'
    @title = "Thank you for feedback."
    @message = "We've sent you <strong>promocode</strong> for next cut to <b>#{@email}</b>."
    @nav_contacts = 'active'

    save_contacts({
        email: @email,
        message: @note,
    })

    erb :message
end


### _admin area
get '/secure/place' do
    #
    login = session[:identity]
    @class = 'alert alert-light'
    @title = 'Admin area'
    @message = "This is a secret place that only priveleged users, like <strong>" + login + "</strong>, has access to!"
    
    erb :admin
end

get '/secure/clients' do
    #
    @class = 'alert alert-light'
    @title = 'Admin area'
    @message = 'Clients tab'
    @tab_clients = 'active'
    @clients = get_clients()
    puts @clients if @debag
    erb :admin
end

get '/secure/contacts' do
    #
    @class = 'alert alert-light'
    @title = 'Admin area'
    @message = 'Contacts tab'
    @tab_contacts = 'active'
    @contacts = get_contacts()
    erb :admin
end

get '/secure/users' do
    #
    @class = 'alert alert-light'
    @title = 'Admin area'
    @message = 'Users tab'
    @tab_users = 'active'
    @users = get_users()
    erb :admin
end

get '/secure/roles' do
    #
    @class = 'alert alert-light'
    @title = 'Admin area'
    @message = 'Roles tab'
    @tab_roles = 'active'
    @roles = get_roles()
    erb :admin
end
###